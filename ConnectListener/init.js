﻿"use strict";

const Log = require("./utils/Log.js");
const http = require('http');
const request = require("request")
const objectid = require('objectid');
const onlineMethod = require("./db/dao/OnlineDao.js").onlineMethod;


function saveData() {
    var nowDate = Date.now;
    var url = 'http://192.168.0.1/goform/getQos?t=' + nowDate;

    Log.info(url);
    request.get({ url: url }, (error, response, body) => {
        if (response && response.statusCode == 200) {
            let obj = JSON.parse(body.toString());
            //Log.info(obj);

            let online = {
                datetime: new Date(),
                users: obj.qosList
            }

            onlineMethod.save(online, (err) => {
                if (!err) {
                    Log.info("存储成功 " + nowDate.datetime);
                } else {
                    Log.error(err);
                }
            })

        } else {
            Log.error(error);
        }
    });
}

module.exports = saveData;