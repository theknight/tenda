﻿"use strict"
const Log = require("./utils/Log.js");
const schedule = require("node-schedule");
const init = require('./init.js');

var j = schedule.scheduleJob(' */1 * * * *', function (fireDate) {
    Log.info("定时器执行：" + new Date());
    init();
});

