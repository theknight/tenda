"use strict";
var Log = require("./utils/Log.js");
var schedule = require("node-schedule");
var init = require('./init.js');
var j = schedule.scheduleJob(' */1 * * * *', function (fireDate) {
    Log.info("定时器执行：" + new Date());
    init();
});
//# sourceMappingURL=app.js.map