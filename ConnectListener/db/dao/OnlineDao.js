﻿var onlineModel = require("../model/Online.js");
var onlineDAO = function () { };

onlineDAO.prototype = {
    //增
    save: function (json, callBack) {
        var newOnline = new onlineModel(json);

        newOnline.save(function (err) {
            callBack(err);
        });
    },
    //删
    remove: function (json, callBack) {
        onlineModel.remove(json, function (err) {
            callBack(err);
        });
    },
    //改
    update: function (json, condition, callBack) {
        var options = { upsert: true };
        onlineModel.update(condition, json, options, function (err) {
            callBack(err);
        });
    },
    //查
    findByName: function (name, callBack) {
        onlineModel.findOne({ qosListHostname: name }, function (err, doc) {
            callBack(err, doc);
        });
    }
};

exports.onlineMethod = new onlineDAO();