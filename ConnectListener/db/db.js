﻿var mongoose = require('mongoose'),
    Log = require("../utils/Log.js"),
    DB_URL = 'mongodb://localhost:27017/router?ssl=false';

/**
 * 连接
 */
mongoose.connect(DB_URL);

/**
  * 连接成功
  */
mongoose.connection.on('connected', function () {
    Log.info('Mongoose connection open to ' + DB_URL);
});

/**
 * 连接异常
 */
mongoose.connection.on('error', function (err) {
    Log.error('Mongoose connection error: ' + err);
});

/**
 * 连接断开
 */
mongoose.connection.on('disconnected', function () {
    Log.info('Mongoose connection disconnected');
});

module.exports = mongoose;