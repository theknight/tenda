﻿var mongoose = require('../db.js'),
    Schema = mongoose.Schema;

var OnlineSchema = new Schema({
    datetime: Date,
    users: [{
        qosListHostname: String,
        qosListRemark: String,
        qosListIP: String,
        qosListConnectType: String,
        qosListMac: String,
        qosListDownSpeed: String,
        qosListUpSpeed: String,
        qosListDownLimit: String,
        qosListUpLimit: String,
        qosListAccess: String
    }]

});

module.exports = mongoose.model('OnlineModel', OnlineSchema);